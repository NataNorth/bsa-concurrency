create table if not exists images(
    id       uuid not null,
    local_url varchar(255),
    hash     bigint,
    primary key (id)
);