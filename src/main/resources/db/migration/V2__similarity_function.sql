CREATE OR REPLACE FUNCTION get_similarity (hash1 bigint, hash2 bigint)
    RETURNS FLOAT AS $$
DECLARE bits bigint := hash1 # hash2;
        counter bigint := 0;
    diffPercent float := 1;
BEGIN
    WHILE ABS(bits) > 0 LOOP
        IF bits % 2 = 1 THEN
            counter := counter + 1;
        END IF;
            bits := bits / 2;

        END LOOP;
    diffPercent := counter/64.;
    RETURN 1 - diffPercent;
END
$$ LANGUAGE plpgsql;