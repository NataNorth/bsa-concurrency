package bsa.java.concurrency;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DHash {
    private static final int hashSize = 8;

    public static long calculate(byte[] imageInBytes) throws IOException {
        InputStream in = new ByteArrayInputStream(imageInBytes);
        BufferedImage bufferedImage = ImageIO.read(in);
        return calculate(bufferedImage);
    }

    public static long calculate(BufferedImage rawImage) {
        long hash = 0;
        var img = preprocessImage(rawImage);
        int wight = img.getWidth() - 1;
        int hight = img.getHeight();
        for (int i = 0; i < wight; i++) {
            for(int j = 0; j < hight; j++) {
                var curr = img.getRGB(i, j);
                var next = img.getRGB(i+1, j);
                hash |= curr > next ? 1 : 0;
                hash <<= 1;
            }
        }
        return hash;
    }

    private static BufferedImage preprocessImage(BufferedImage img) {
        int newWidth = hashSize + 1;
        int newHeight = hashSize;
        Image tmp = img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
        BufferedImage result = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_BYTE_GRAY);
        result.getGraphics().drawImage(tmp, 0, 0, null);
        return result;
    }
}
