package bsa.java.concurrency.fs;

import antlr.StringUtils;
import bsa.java.concurrency.fs.exception.FileStorageException;
import bsa.java.concurrency.image.entity.Image;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.Future;

@Service
public class FileSystemService {

    private static final Logger logger = LoggerFactory.getLogger(FileSystemService.class);

    @Value("${spring.resources.static-locations}")
    private String localUrl;

    @Value("${server.port}")
    private int port;

    private String storage = "src/main/resources/images";

    private final Path fileStorageLocation;

    @Autowired
    public FileSystemService() {
        var path = System.getProperty("user.dir") + "/" + storage;

        fileStorageLocation = Path.of(path);
        try {
            Files.createDirectories(fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored", ex);
        }
        logger.info("Hard drive memory storage folder was created");
    }

    @Async
    public Future<Image> storeImage(MultipartFile file) throws IOException {
        UUID id = UUID.randomUUID();
        String fileName = id + "." + FilenameUtils.getExtension(file.getOriginalFilename());

        Path targetLocation = fileStorageLocation.resolve(fileName);
        Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

        String url = "http://localhost:" + port + "/" + fileName;
        Image image = Image.builder().id(id).localUrl(url).build();
        return new AsyncResult<>(image);
    }

    public void deleteById(UUID deleteId) {
        File folder = new File(storage);
        for (File image : Objects.requireNonNull(folder.listFiles())) {
            String id = FilenameUtils.removeExtension(image.getName());
            if (id.equals(deleteId.toString())) {
                image.delete();
            }
        }
    }

    public void delete() {
        String path = Path.of(storage).toAbsolutePath().toString();
        File folder = new File(path);
        for (File image : Objects.requireNonNull(folder.listFiles())) {
            image.delete();
        }
    }
}
