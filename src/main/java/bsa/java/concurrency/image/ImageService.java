package bsa.java.concurrency.image;

import bsa.java.concurrency.DHash;
import bsa.java.concurrency.image.ImageRepository;
import bsa.java.concurrency.fs.FileSystemService;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Service
public class ImageService {

    private final Logger logger = LoggerFactory.getLogger(FileSystemService.class);

    @Autowired
    FileSystemService fileSystemService;

    @Autowired
    ImageRepository imageRepository;

    public void uploadImages(MultipartFile[] files) throws InterruptedException {
        var list = List.of(files);
        list.parallelStream().forEach(this::store);
    }

    public List<SearchResultDTO> search(MultipartFile file, double threshold) throws IOException, ExecutionException, InterruptedException {
        long hash = DHash.calculate(file.getBytes());
        var response = imageRepository.getSearchResult(hash, threshold);
        if (response.isEmpty()) {
            Future<Image> future = fileSystemService.storeImage(file);
            while (true) {
                if (future.isDone()) {
                    Image image = future.get();
                    image.setHash(hash);
                    imageRepository.save(image);
                    break;
                }
            }
        }
        return response;
    }

    private void store(MultipartFile file) {
        Image image = null;
        try {
            Future<Image> future = fileSystemService.storeImage(file);
            long hash = DHash.calculate(file.getBytes());

            while (true) {
                if (future.isDone()) {
                    image = future.get();
                    image.setHash(hash);
                    break;
                }
            }
        } catch (IOException | ExecutionException | InterruptedException ex) {
            logger.error("Storage error", ex);
        }
        assert image != null; //TODO
        imageRepository.save(image);
    }

    public void deleteById(UUID id) {
        imageRepository.deleteById(id);
        fileSystemService.deleteById(id);
    }

    public void delete() {
        imageRepository.deleteAll();
        fileSystemService.delete();
    }
}
