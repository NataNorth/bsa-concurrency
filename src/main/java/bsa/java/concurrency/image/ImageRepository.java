package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ImageRepository extends JpaRepository<Image, UUID> {
    @Query(value = "SELECT cast(i.id as varchar) as imageId, " +
            "i.local_url as imageUrl, " +
            "get_similarity(i.hash, :hash)*100 as matchPercent " +
            "FROM images i WHERE get_similarity(i.hash, :hash) > :threshold",
            nativeQuery = true)
    List<SearchResultDTO> getSearchResult(long hash, double threshold);

    void deleteById(UUID id);

    void deleteAll();
}