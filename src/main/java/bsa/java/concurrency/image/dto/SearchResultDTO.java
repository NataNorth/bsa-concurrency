package bsa.java.concurrency.image.dto;

import java.util.UUID;

public interface SearchResultDTO {
    String getImageId();
    Double getMatchPercent();
    String getImageUrl();
}
